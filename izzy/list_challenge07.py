# Write three functions that compute the sum of the numbers in a list:
# using a for-loop, a while-loop and recursion. (Subject to availability of these constructs in your language of choice.)

from random import randint

def for_sum(collection):
	total_sum = 0
	for num in collection:
		total_sum += num
	return total_sum


def while_sum(collection):
	total_sum = 0
	index = 0
	while index < len(collection):
		total_sum += collection[index]
		index += 1
	return total_sum

demo_list = [randint(1,100) for _ in range(10)]

print(demo_list)

print(for_sum(demo_list))

print(while_sum(demo_list))
