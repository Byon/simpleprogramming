# Write a program that prints all prime numbers.
# (Note: if your programming language does not support arbitrary size numbers,
# printing all primes up to the largest number you can easily represent is fine too.)

high_number = 100000

prime_number_list = []
prime_numbers_found = 0

print(f"Printing all the prime numbers up to: {high_number}")

for num in range(2, high_number + 1):
	# go one by one through each number and check their divisibles

	divisibles = []

	for i in range(2, num):
		if (num % i == 0):
			divisibles.append(i)
			continue
	if len(divisibles) == 0:
		prime_number_list.append(num)
		print(num)
		prime_numbers_found += 1

print(f"There are a total of {len(prime_number_list)} prime numbers in that range.")

