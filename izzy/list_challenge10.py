# Write a function that combines two lists by alternatingly taking elements, e.g. [a,b,c], [1,2,3] → [a,1,b,2,c,3].

def alternate_combine_lists(collection1, collection2):
	list1_length = len(collection1)
	list2_length = len(collection2)

	new_list = []

	index = 0
	while index < min(list1_length, list2_length):
		new_list.append(collection1[index])
		new_list.append(collection2[index])
		index += 1
	return new_list

demo_list1 = ["a","b","c", "d"]
demo_list2 = [1,2,3]	

test_list = alternate_combine_lists(demo_list1, demo_list2)
print(test_list)
