# Write a program that prints all prime numbers.
# (Note: if your programming language does not support arbitrary size numbers,
# printing all primes up to the largest number you can easily represent is fine too.)

# This version doesn't keep a list of prime numbers in memory, so it's faster.
# It also stops testing numbers once it hits a divisible and moves on to the next number.

# should result in 9592 prime numbers between 2 and 100000

import math

high_number = 100000

num_of_prime_numbers_found = 0

print(f"Printing all the prime numbers up to: {high_number}")

for num in range(2, high_number + 1):
	# print(f"testing {num}")
	# go one by one through each number and check their divisibles

	max_possible_factor = math.ceil(num**0.5)
	# print(f"This is the max possible factor: {max_possible_factor}")

	testing_num = True

	i = 2
	while testing_num:
		if num % i == 0:
			# not prime, test next number
			testing_num = False
		else:
			i += 1
		if i > num ** 0.5:
			# num must be a prime
			print(f"{num}")
			num_of_prime_numbers_found += 1
			testing_num = False
		

print(f"There are a total of {num_of_prime_numbers_found} prime numbers in the range of 2 to {high_number}.")
