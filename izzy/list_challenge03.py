# Write a function that checks whether an element occurs in a list.

from random import randint

def is_in_list(value, collection):
	value_present = False
	for item in collection:
		if item == value:
			value_present = True
			break	
	return value_present


test_list = [randint(0,100) for _ in range(100)]
test_value = randint(0,100)

print(test_list)
print("\n")
print(f"{test_value} is in list? {is_in_list(test_value, test_list)}")

