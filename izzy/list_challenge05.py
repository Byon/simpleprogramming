# Write a function that computes the running total of a list.

from random import randint

def compute_running_total(collection):
	running_total = 0
	for i, num in enumerate(collection):
		running_total += num
		print(f"The total at position {i} is {running_total}.")
	return running_total

demo_list = [randint(0,100) for _ in range(10)]
print(demo_list)

compute_running_total(demo_list)
