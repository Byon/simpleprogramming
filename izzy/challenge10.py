# Write a program that prints the next 20 leap years.
# The year can be evenly divided by 4;
# If the year can be evenly divided by 100, it is NOT a leap year, unless;
# The year is also evenly divisible by 400. Then it is a leap year.


current_year = 2019

count = 0
leap_years = []
while len(leap_years) < 20:
	if current_year % 4 == 0:
		if current_year % 100 == 0 and current_year % 400 == 0:
			leap_years.append(current_year)
		elif current_year % 100 == 0:
			# not a leap year
			current_year += 1
			continue
		leap_years.append(current_year)
	current_year += 1

print(leap_years)

