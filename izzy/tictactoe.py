# Tic Tac Toe program based on Udemy python training

def print_board(board_state):

	print("\n")
	print("This is what the board looks like now:")
	print("\n")

	print("   |   |   ")
	print(f" {board_state['7']} | {board_state['8']} | {board_state['9']} ")
	print("   |   |   ")
	print("-----------")
	print("   |   |   ")
	print(f" {board_state['4']} | {board_state['5']} | {board_state['6']} ")
	print("   |   |   ")
	print("-----------")
	print("   |   |   ")
	print(f" {board_state['1']} | {board_state['2']} | {board_state['3']} ")
	print("   |   |   ")
	print("\n")



def take_turn(board_state, current_player):
	while True:
		print("")
		player_move = input(f"{current_player['name']} ({current_player['symbol']}): Where do you want to go? ")
		if player_move not in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
			print("Please choose a valid position, an empty space from 1-9.")
			continue
		else:
			if board_state[player_move] == "X" or board_state[player_move] == "O":
				print("Sorry, that space is already filled. Please choose another space.")
			else:
				board_state[player_move] = current_player["symbol"]
				break

	return (board_state, current_player)

def win(current_player):
	print(f"{current_player['name']} wins!")

def check_for_win(board_state, current_player):
	letter = current_player["symbol"]
	
	if board_state["1"] == letter and board_state["2"] == letter and board_state["3"] == letter:
		win(current_player)
		return True
	elif board_state["4"] == letter and board_state["5"] == letter and board_state["6"] == letter:
		win(current_player)
		return True
	elif board_state["7"] == letter and board_state["8"] == letter and board_state["9"] == letter:
		win(current_player)
		return True
	elif board_state["1"] == letter and board_state["4"] == letter and board_state["7"] == letter:
		win(current_player)
		return True
	elif board_state["2"] == letter and board_state["5"] == letter and board_state["8"] == letter:
		win(current_player)
		return True
	elif board_state["3"] == letter and board_state["6"] == letter and board_state["9"] == letter:
		win(current_player)
		return True
	elif board_state["1"] == letter and board_state["5"] == letter and board_state["9"] == letter:
		win(current_player)
		return True
	elif board_state["3"] == letter and board_state["5"] == letter and board_state["7"] == letter:
		win(current_player)
		return True
	elif " " not in board_state.values():
		print("It's a tie!!!!")
		return True
	else:
		pass
	return False


def reset_board():
	board_state = {"1": " ", "2": " ", "3": " ", "4": " ", "5":" ", "6":" ", "7":" ", "8":" ", "9":" "}
	return board_state





	
print("\n"*30)
print("Welcome to Tic-Tac-Toe!")
print("\n")

player1 = {}
player2 = {}
player1["name"] = "Player 1"
player2["name"] = "Player 2"


while True:
	player1["symbol"] = input(f"{player1['name']}: Do you want to be 'X' or 'O'? ")
	if player1["symbol"] == "X" or player1["symbol"] == "O":
		break
	else:
		print("Please choose 'X' or 'O' only.")



# resolve symbols for players
if player1["symbol"] == "X":
	player2["symbol"] = "O"
else:
	player2["symbol"] = "X"


# set up board and scores
board_state = {"1": " ", "2": " ", "3": " ", "4": " ", "5":" ", "6":" ", "7":" ", "8":" ", "9":" "}
print_board(board_state)


current_player = player1

while True:
	take_turn(board_state, current_player)
	print_board(board_state)
	someone_won = check_for_win(board_state, current_player)
	if someone_won:
		replay = input("Would you like to play again? y/n ").lower()
		if replay != "y":
			print("\n")
			print("Thanks for playing! Goodbye!")
			break
		else:
			board_state = reset_board()
			print("\n"*30)
			print_board(board_state)
	if current_player == player1:
		current_player = player2
	elif current_player == player2:
		current_player = player1

