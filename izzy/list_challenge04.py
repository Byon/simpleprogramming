# Write a function that returns the elements on odd positions in a list.
from random import randint

demo_list = [randint(0,100) for _ in range(10)]

def return_odd_elements(collection):
	new_list = []
	for i,value in enumerate(collection):
		if i == 1 or i % 2 == 1:
			new_list.append(value)
	return new_list

print(demo_list)
odd_list = return_odd_elements(demo_list)
print(odd_list)
