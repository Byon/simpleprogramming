# Write function that reverses a list, preferably in place.

from random import randint

def reverse_list(original_list):
	return original_list[::-1]


test_list = [randint(0,100) for _ in range(10)]

print(test_list)

print(reverse_list(test_list))

# Not sure how to reverse a list in place without
# using the .reverse() method that's already built into python lists.