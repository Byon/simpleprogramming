# Write a function that returns the largest element in a list.

my_list = [8, 3, 5, 16, 2, 5, 2, 98, 4, 3, 5]


def get_largest_element(list_of_values):

	largest_element = list_of_values[0]
	for value in list_of_values:
		if value > largest_element:
			largest_element = value

	return largest_element

print(get_largest_element(my_list))