# Write a function that tests whether a string is a palindrome.

def is_palindrome(term):
	return term.lower() == term[::-1].lower()


test_term = input("Give me a string and I'll test whether it's a palindrome: ")

if is_palindrome(test_term):
	print("Yes, that's a palindrome.")
else:
	print("No, that's not a palindrome.")

