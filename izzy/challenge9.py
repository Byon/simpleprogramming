# Write a guessing game where the user has to guess a secret number.
# After every guess the program tells the user whether their number
# was too large or too small. At the end the number of tries needed
# should be printed. It counts only as one try if they input the
# same number multiple times consecutively
from random import randint

secret_number = randint(1,50)
print("Hey there. Computer here. I picked a number between 1 and 50. Try to guess it.")
guesses = []
while True:
	guess_num = int(input("Your guess? "))
	if len(guesses) == 0 or guess_num != guesses[-1]:
		guesses.append(guess_num)

	if guess_num == secret_number:
		print(f"You win! Congrats! And it only took you {len(guesses)} guesses!")
		break
	elif guess_num > secret_number:
		print("Too high!")
	elif guess_num < secret_number:
		print("Too low!")
	else:
		print("Please guess an integer number.")

print("Thanks for playing!")
