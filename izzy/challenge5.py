# Write a program that asks the user for a number n and prints the sum of the numbers 1 to n

number = int(input("Hey, can you give me an integer number please? (Big ones are good ones.): "))

running_total = 0

for num in range(1,number+1):
	if num % 3 == 0 or num % 5 == 0:
		running_total += num
	else:
		continue

print(f"The grand total is {running_total}.")
