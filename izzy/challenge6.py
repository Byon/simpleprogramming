number = int(input("Can you please give me an integer number (it's for my magic trick?) :"))

def check_preference():
	preference = input("And do you prefer I sum all the numbers or multiply? (enter 'sum' or 'multiply') : ").lower()
	return preference


program_running = True

while program_running:
	preference = check_preference()

	if preference == "sum":
		running_sum = 0
		for num in range(1,number+1):
			running_sum += num
		print(f"The final sum is {running_sum}.")
		program_running = False

	elif preference == "multiply":
		running_product = 1
		for num in range(1,number+1):
			running_product *= num
		print(f"The final product is {running_product}.")
		program_running = False

	else:
		print("Sorry, I didn't understand the answer...")
		

