# Write a function that concatenates two lists. [a,b,c], [1,2,3] → [a,b,c,1,2,3]

def combine_lists(collection1, collection2):
	return collection1 + collection2

list1 = ["a","b","c"]
list2 = [1,2,3]

print(combine_lists(list1, list2))