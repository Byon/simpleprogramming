# Write a program that prints all prime numbers.
# (Note: if your programming language does not support arbitrary size numbers,
# printing all primes up to the largest number you can easily represent is fine too.)

# This version doesn't keep a list of prime numbers in memory, so it's faster.
# It also stops testing numbers once it hits a divisible and moves on to the next number.

# should result in 9592 prime numbers between 2 and 100000

high_number = 100000

num_of_prime_numbers_found = 0

print(f"Printing all the prime numbers up to: {high_number}")

for num in range(2, high_number + 1):
	# go one by one through each number and check their divisibles

	for i in range(2, num+1):
		if (num % i == 0) and (i < num) :
			# not prime, go to next num
			break
		if i == num:
			# num is prime
			print(num)
			num_of_prime_numbers_found += 1

print(f"There are a total of {num_of_prime_numbers_found} prime numbers in the range of 2 to {high_number}.")
