import random

no_switch_wins = 0
no_switch_losses = 0
no_switch_total_tries = 0

switch_wins = 0
switch_losses = 0
switch_total_tries = 0

# test no switch scenario
for _ in range(0,10000):
	# Setup round:
	doors = ["goat", "goat", "car"]
	all_door_options = [0,1,2]
	random.shuffle(doors)
	print("\n"*5)
	print(doors)

	# computer player chooses random door
	choice = random.randint(0,2)
	print(f"Player chooses door: {choice}")
	index_of_player_door_in_list = all_door_options.index(choice)
	all_door_options.pop(index_of_player_door_in_list)



	# tease player
	indexes_of_goats = []
	for i, prize in enumerate(doors):
		if choice == i:
			continue
		elif doors[i] == "goat" and i != choice:
			opened_door = i
			index_of_opened_door_in_list = all_door_options.index(opened_door)
			all_door_options.pop(index_of_opened_door_in_list)
			print(all_door_options)
			print(f"Opening door number {i} to reveal a {doors[i]}.")
			print("Keeps current door")
			break

	
	# determine the outcome
	result = doors[choice]
	print(f"The player selected a : {result}")
	if result == "goat":
		print("Sorry for your loss!")
		no_switch_losses += 1
		no_switch_total_tries += 1
	elif result == "car":
		print("Congrats on your win!")
		no_switch_wins += 1
		no_switch_total_tries += 1
	else:
		print("something went wrong")



# test no switch scenario
for _ in range(0,10000):
	# Setup round:
	doors = ["goat", "goat", "car"]
	all_door_options = [0,1,2]
	random.shuffle(doors)
	print("\n"*5)
	print(doors)

	# player chooses random door
	choice = random.randint(0,2)
	print(f"Player chooses door: {choice}")
	index_of_player_door_in_list = all_door_options.index(choice)
	all_door_options.pop(index_of_player_door_in_list)



	# tease player
	indexes_of_goats = []
	for i, prize in enumerate(doors):
		if choice == i:
			continue
		elif doors[i] == "goat" and i != choice:
			opened_door = i
			index_of_opened_door_in_list = all_door_options.index(opened_door)
			all_door_options.pop(index_of_opened_door_in_list)
			print(all_door_options)
			print(f"Opening door number {i} to reveal a {doors[i]}.")
			print("Would you like to keep your current door or switch to the other one? Yes.")
			break

	
	
	print("The only door option left is: {}".format(all_door_options))
	choice = all_door_options[0]
	print(f"The player just switched to door: {choice}")



	# determine the outcome
	result = doors[choice]
	print(f"The player selected a : {result}")
	if result == "goat":
		print("Sorry for your loss!")
		switch_losses += 1
		switch_total_tries += 1
	elif result == "car":
		print("Congrats on your win!")
		switch_wins += 1
		switch_total_tries += 1
	else:
		print("something went wrong")

# Print Results
print("\n"*2)

print("Total no_switch_wins: {}".format(no_switch_wins))
print("Total no_switch_losses: {}".format(no_switch_losses))
print("Player won: {} of the time.".format(no_switch_wins/no_switch_total_tries))

print("\n"*2)

print("Total switch_wins: {}".format(switch_wins))
print("Total switch_losses: {}".format(switch_losses))
print("Player won: {} of the time.".format(switch_wins/switch_total_tries))

