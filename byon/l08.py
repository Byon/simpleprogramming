def print_square( n ):
    print( n, "squared is", n*n )

def on_all( l, f ):
    for i in l:
        f( i )

on_all( range(1,20+1), print_square )
