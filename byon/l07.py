def for_sum( l ):
    sum = 0
    for i in range(len(l)):
        sum += l[i]
    return sum

def while_sum( l ):
    sum = i = 0
    while i < len(l):
        sum += l[i]
        i += 1
    return sum

def recurse_sum( l ):
    if not l:
        return 0
    return l[0] + recurse_sum(l[1:])

from random import randint
l = [randint(0,20) for i in range(10)]
print( l )

print( for_sum(l) )
print( while_sum(l) )
print( recurse_sum(l) )
