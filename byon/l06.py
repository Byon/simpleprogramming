def is_palindrome_old( str ):
    l = len(str)
    for i in xrange(l/2):
        if str[i] != str[l-1-i]:
            return False
    return True

def reverse( str ):
    return str[::-1]
#    return ''.join(reversed(str))

def is_palindrome( str ):
    return str == reverse(str)

strings = [ "ABCCBA", "ABCBA", "ABCCBB"]
for str in strings:
    print( str, "is" if is_palindrome(str) else "is NOT", "a palindrome" )
