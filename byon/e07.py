max = 12
line = ' '*2 + '|'
for x in range( 1, max+1 ):
    line += "{:4d}".format(x)
print( line )
print( '-'*2 + '+' + '-'*max*4 )
for y in range( 1, max+1 ):
    line = "{:2d}".format(y) + '|'
    for x in range( 1, max+1 ):
        line += "{:4d}".format(x*y)
    print( line )
    
