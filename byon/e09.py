from random import randrange

max = 10
answer = randrange( max ) + 1
print( "Guess the number between 1 and", max )

guess, guesses = 0, []
while guess != answer:
    guess = int(input( "Make a guess:" ))
    if guess in guesses:
        print( "You already guessed that!" )
    else:
        guesses.append( guess )
    if guess < answer:
        print( guess, "is too low" );
    if guess > answer:
        print( guess, "is too high" );

print( guess, "is Correct!" );
print( "You made", len(guesses), "guesses" );
