def my_occurs( l, element ):
    for i in l:
        if i==element:
            return True
    return False

from random import randint
l = [randint(0,20) for i in range(10)]
print( l )
find = 10

print( find, "Is" if (my_occurs( l, find )) else "Not", "Found" )

print( find, "Is" if (find in l) else "Not", "Found" )
