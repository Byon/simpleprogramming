def merge_sort( a, b ):
    out = []
    while a or b:
        if a and (not b or a[0] < b[0]):
            out.append( a.pop(0) )
        elif b:
            out.append( b.pop(0) )
    return out

list1 = [1,4,6]
list2 = [2,3,5]
print( merge_sort( list2, list1) )
