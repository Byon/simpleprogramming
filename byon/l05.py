def odd_indices_1( l ):
    out = []
    include = False
    for i in l:
        if include:
            out.append(i)
        include = not include
    return out

def odd_indices_2( l ):
    out = []
    for i in range(1,len(l),2):
        out.append(l[i])
    return out

from random import randint
l = [randint(0,20) for i in range(10)]
print( l )

print( odd_indices_1(l) )
print( odd_indices_2(l) )
print( [l[i] for i in range(1,len(l),2)] )
print( l[1::2] )
