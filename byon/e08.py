primes = []                     # start with an empty list of primes
n = 1                           # set the last number checked
while True:                     # run forever
    n = n+1                     # advance to the next number to check
    is_prime = True             # assume it is prime
    for p in primes:            # loop over all previous primes
        if n%p==0:              # if the prime evenly divides the number
            is_prime = False    #  the number is NOT prime
            break               #  don't need to check any more primes
        if p*p>n:               # if the current prime > sqrt of the number
            break               #  don't need to check any more primes
    if is_prime:                # if we didn't prove the number wasn't a prime
        primes.append( n )      #  add it to the prime list
        print( n )              #  print the new prime
