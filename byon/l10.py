def combine( a, b ):
    out = []
    for i in range( max( len(a), len(b) ) ):
        if i < len(a):
            out.append( a[i] )
        if i < len(b):
            out.append( b[i] )
    return out

list1 = ['a','b','c']
list2 = [1,2,3]
print( combine( list1, list2 ) )
