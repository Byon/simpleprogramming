def my_reverse( l ):
    for i in range( int(len(l)/2) ):
        temp = l[i]
        l[i] = l[-1-i]
        l[-1-i] = temp
    return l

from random import randint
l = [randint(0,100) for i in range(9)]
print( l )

print( my_reverse(l) )

l.reverse()
print( l )
