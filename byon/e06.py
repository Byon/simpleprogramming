number = int(input( "Please enter a number:" ))
calc_sum = input( "Select sum or product:" )[0]=='s'
answer = 0 if calc_sum else 1
for i in range( 1, number+1 ):
    if calc_sum:
        answer += i
    else:
        answer *= i
print( "Sum" if calc_sum else "Product", "from 1 to", number, "is", answer )
