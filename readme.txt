Simple Programming Project

The goal:
  To familiarize beginners with basic programming and version control practices

Instructions:
  Install git & learn some git basics
  Install the project with: git clone git@bitbucket.org:Byon/simpleprogramming.git
  Create your personal directory
  Let Byon know your bitbucket.org details so you can be added to the user list
  Commit your update to the repo (see git.txt)
  Install python & learn some python basics
  Solve some of the programming problems
  See how others solved the same problems
  Commit your code so others can learn from it
